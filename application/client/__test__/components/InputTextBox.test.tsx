import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { InputTextBox } from '../../components/InputTextBox';
import { CTX as AppContext } from '../../appContext';
import '@testing-library/jest-dom/extend-expect';
import { IPayload, IChatMessage } from '../../appContext/reducers';

describe('InputTextBox behaviour', () => {
  const InputTextBoxWithMockedContext = () => {
    const initialState = {
      userInteraction: {},
      connectionInfo: {
        connectedWith: {
          avatar: '',
        },
      },
    } as IPayload;
    const { userInteraction, connectionInfo } = initialState;
    const messages = [] as IChatMessage[];
    const dispatch = jest.fn();

    return (
      <AppContext.Provider value={{ userInteraction, connectionInfo, messages, dispatch }}>
        <InputTextBox />
      </AppContext.Provider>
    );
  };

  it('should leave inputText empty after pressing Enter key', async () => {
    const { getByPlaceholderText } = render(<InputTextBoxWithMockedContext />);
    const inputElement = getByPlaceholderText('write a message here');

    inputElement.focus();
    fireEvent.change(inputElement, { target: { value: 'H' } });
    fireEvent.change(inputElement, { target: { value: 'i' } });
    fireEvent.keyUp(inputElement, { key: 'Enter', code: 13 });

    // @ts-ignore
    expect(inputElement.value).toBe('');
  });

  it('should show an arrow icon if the user type something', async () => {
    const { container, getByPlaceholderText } = render(<InputTextBoxWithMockedContext />);
    const inputElement = getByPlaceholderText('write a message here');

    inputElement.focus();
    fireEvent.change(inputElement, { target: { value: 'H' } });

    const arrowIcon = container.querySelector('span[class="arrow"]');

    expect(arrowIcon).toBeInTheDocument();
  });

  it('should remove arrow icon if user leave inputText empty', async () => {
    const { container, getByPlaceholderText } = render(<InputTextBoxWithMockedContext />);
    const inputElement = getByPlaceholderText('write a message here');
    let arrowIcon;

    inputElement.focus();

    fireEvent.change(inputElement, { target: { value: 'a' } });
    arrowIcon = container.querySelector('span[class="arrow"]');
    expect(arrowIcon).toBeInTheDocument();

    fireEvent.change(inputElement, { target: { value: '' } });
    arrowIcon = container.querySelector('span[class="arrow"]');

    expect(arrowIcon).toBeNull();
  });
});
