import React from 'react';
import { render } from '@testing-library/react';
import { Header } from '../../components/Header';
import { IPayload, IChatMessage } from '../../appContext/reducers';
import { CTX as AppContext } from '../../appContext';
import '@testing-library/jest-dom/extend-expect';

describe('Header typing status behaviour', () => {
  function setProviderContextWithTypingValueIn(isTyping: boolean) {
    const initialState = {
      userInteraction: {
        isTyping,
      },
      connectionInfo: {
        connectedWith: {
          avatar: '',
        },
      },
    } as IPayload;
    const { userInteraction, connectionInfo } = initialState;
    const messages = [] as IChatMessage[];
    const dispatch = jest.fn();

    return () => (
      <AppContext.Provider value={{ messages, userInteraction, connectionInfo, dispatch }}>
        <Header />
      </AppContext.Provider>
    );
  }

  it('should show "typing..." status text', () => {
    const HeaderWithMockedContext = setProviderContextWithTypingValueIn(true);
    const { getByText } = render(<HeaderWithMockedContext />);

    expect(getByText('typing...')).toBeInTheDocument();
  });

  it('should show "online" status text', () => {
    const HeaderWithMockedContext = setProviderContextWithTypingValueIn(false);
    const { getByText } = render(<HeaderWithMockedContext />);

    expect(getByText('online')).toBeInTheDocument();
  });
});
