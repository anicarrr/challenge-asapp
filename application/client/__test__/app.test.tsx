import React from 'react';
import ReactDOM from 'react-dom';
import { App } from '../app';
import { IUser } from '../../models/user';

it('renders without crashing', () => {
  const User1: IUser = {
    avatar: 'some/path',
    name: 'Test1',
    userId: 'A123',
  };

  const User2: IUser = {
    avatar: 'some/path',
    name: 'Test2',
    userId: 'B123',
  };

  const div = document.createElement('div');
  ReactDOM.render(<App currentUser={User1} connectedWith={User2} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
