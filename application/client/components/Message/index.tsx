import React from 'react';
import styles from './styles.scss';
import classNames from 'classnames';

function Message({ direction, children }: Props) {
  const isLeft = direction === 'left';
  const position = isLeft ? styles.toLeft : styles.toRight;
  const response = isLeft && styles.response;
  const border = isLeft ? styles.othersTalking : styles.meTalking;

  return (
    <div className={classNames(styles.spacing, position)}>
      <div className={classNames(styles.container, response, border)}>
        {children}
      </div>
    </div>
  );
}

export { Message };

interface Props {
  direction: 'left' | 'right';
  children: JSX.Element | JSX.Element[] | string;
}
