import React, { useState, ChangeEvent, KeyboardEvent, useContext, useMemo, useRef } from 'react';
import styles from './styles.scss';
import { CTX as AppContext } from '../../appContext';
import once from 'lodash/once';
import debounce from 'lodash/debounce';
import { sendMessage as sendMessageAction } from '../../appContext/actionCreators';
import { SocketClient } from '../../socketClient';

function InputTextBox() {
  const [message, setMessage] = useState('');
  const initTypingCycle = useRef(0);
  const { dispatch, connectionInfo } = useContext(AppContext);

  /**
   * This guarantee initTypingEvent function to be call once until initTypingCycle variable change
   */

  const initTypingEvent = useMemo(() => once(() => dispatchIsTypingWithValue(true)), [
    once,
    initTypingCycle.current,
    connectionInfo.connectedWith.userId,
  ]);

  /**
   * This function is call only when the user stop typing for 500ms
   */

  const debounceTypingEvent = useMemo(
    () =>
      debounce(() => {
        dispatchIsTypingWithValue(false);
        getReadyForNextTypingEvent();
      }, 500),
    [debounce, connectionInfo.connectedWith.userId],
  );

  /**
   * we update initTypingCycle variable to be
   * ready for the next typing cycle
   */

  function getReadyForNextTypingEvent() {
    initTypingCycle.current += 1;
  }

  function dispatchIsTypingWithValue(isTyping: boolean) {
    SocketClient.sendTypingStatus(isTyping, connectionInfo.connectedWith.userId);
  }

  function handleChange(e: ChangeEvent<HTMLInputElement>) {
    setMessage(e.target.value);
  }

  function handleKeyPress(event: KeyboardEvent) {
    if (message.length && event.key === 'Enter') {
      sendMessage();
    } else {
      initTypingEvent();
      debounceTypingEvent();
    }
    event.preventDefault();
    event.stopPropagation();
  }

  function sendMessage() {
    dispatch(sendMessageAction(message, connectionInfo.connectedWith.userId));
    setMessage('');

    /**
     * we cancel the debounce function to avoid
     * sync issues when user press Enter
     */

    debounceTypingEvent.cancel();
    getReadyForNextTypingEvent();
  }

  return (
    <>
      <input
        placeholder="write a message here"
        value={message}
        onKeyUp={handleKeyPress}
        onChange={handleChange}
        className={styles.inputContainer}
        maxLength={5000}
      />
      {message && <span onClick={sendMessage} className={styles.arrow} />}
    </>
  );
}

export { InputTextBox };
