import React from 'react';
import styles from './styles.scss';

function Avatar({ img }: Props) {
  return <img src={img} className={styles.imgCircular} />;
}

export { Avatar };

interface Props {
  img: string;
}
