import { useEffect, useContext } from 'react';
import SocketManager from '../../../fakeServer/socketManager';
import { CTX as appContext } from '../../appContext';
import { addMessageToChat, notifyTypingStatus } from '../../appContext/actionCreators';

function ChatEventListeners() {
  const { connectionInfo, dispatch } = useContext(appContext);
  const { currentUser } = connectionInfo;
  const { userId } = currentUser;

  useEffect(() => {
    const sub = SocketManager.subscribe(`NEW_OUTSIDER_MESSAGE__${userId}`, (message: string) => {
      dispatch(addMessageToChat(message));
    });

    return () => {
      sub.unsubscribe();
    };
  }, [currentUser]);

  useEffect(() => {
    const sub = SocketManager.subscribe(`SOMEONE_IS_TYPING_ME__${userId}`, (isTyping: boolean) => {
      dispatch(notifyTypingStatus(isTyping));
    });

    return () => {
      sub.unsubscribe();
    };
  }, [currentUser]);

  return null;
}

export { ChatEventListeners };
