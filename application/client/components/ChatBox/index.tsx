import React, { useContext, useRef, useEffect, useMemo } from 'react';
import styles from './styles.scss';
import classNames from 'classnames';
import { CTX as AppContext } from '../../appContext';
import { Message } from '../Message';

function ChatBox() {
  const { messages } = useContext(AppContext);
  const elementRef = useRef(null);

  useEffect(() => {
    /**
     * We scroll to bottom when a new message shows up
     */

    // @ts-ignore
    elementRef.current.scrollTop = elementRef.current.scrollHeight;
  }, [messages.length]);

  return useMemo(
    () => (
      <div ref={elementRef} className={classNames(styles.container, styles.contentReverse)}>
        {messages.map(item => (
          <Message key={item.id} direction={item.isMe ? 'right' : 'left'}>
            {item.message}
          </Message>
        ))}
      </div>
    ),
    [messages],
  );
}

export { ChatBox };
