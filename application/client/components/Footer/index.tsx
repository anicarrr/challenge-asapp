import React from 'react';
import styles from './styles.scss';
import classNames from 'classnames';

function Footer({ children }: Props) {
  return <div className={classNames(styles.bg, 'd-flex')}>{children}</div>;
}

export { Footer };

interface Props {
  children: JSX.Element | JSX.Element[];
}
