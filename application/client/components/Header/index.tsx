import React, { useContext, memo } from 'react';
import styles from './styles.scss';
import classNames from 'classnames';
import { CTX as AppContext } from '../../appContext';
import { Avatar } from '../Avatar';

const AvatarMemoized = memo(({ avatar }: Props) => <Avatar img={avatar} />);

function Header() {
  const { userInteraction, connectionInfo } = useContext(AppContext);
  const { isTyping } = userInteraction;
  const { connectedWith } = connectionInfo;

  return (
    <div className={classNames(styles.container, 'd-flex')}>
      <AvatarMemoized avatar={connectedWith.avatar} />
      <div className={styles.userInfo}>
        <div>
          <span className={classNames(styles.userTitle, styles.cleanText)}>{connectedWith.name}</span>
        </div>
        <div>
          <span className={classNames(styles.userStatus, styles.cleanText)}>
            {isTyping ? 'typing...' : 'online'}
          </span>
        </div>
      </div>
    </div>
  );
}

export { Header };

interface Props {
  avatar: string;
}
