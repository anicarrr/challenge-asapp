import React from 'react';
import { ChatBox } from './components/ChatBox';
import { Footer } from './components/Footer';
import { InputTextBox } from './components/InputTextBox';
import { AppContext } from './appContext';
import { ChatEventListeners } from './components/ChatEventListeners';
import { Header } from './components/Header';
import { IUser } from '../models/user';
import './styles.global.scss';

function App({ currentUser, connectedWith }: Props) {
  return (
    <AppContext currentUser={currentUser} connectedWith={connectedWith}>
      <ChatEventListeners />
      <Header />
      <ChatBox />
      <Footer>
        <InputTextBox />
      </Footer>
    </AppContext>
  );
}

export { App };

interface Props {
  currentUser: IUser;
  connectedWith: IUser;
}
