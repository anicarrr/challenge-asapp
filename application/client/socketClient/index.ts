import SocketManager from '../../fakeServer/socketManager';

function sendMessage(message: string, to: string) {
  SocketManager.emit('NEW_MESSAGE_EVENT', {
    message,
    to,
  });
}

function sendTypingStatus(isTyping: boolean, to: string) {
  SocketManager.emit('NOTIFY_TYPING_EVENT', {
    isTyping,
    to,
  });
}

const SocketClient = {
  sendMessage,
  sendTypingStatus,
};

export { SocketClient };
