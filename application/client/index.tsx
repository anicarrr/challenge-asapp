import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './app';

import '../fakeServer/socketEvents'; /* <-- We import this to make it part of the bundle */
import { user1, user2 } from '../fakeServer/getUsers';

/**
 * As the challenge only mention the instance where Rob is already connected
 * and chatting with Laura I ommited the user authentication and chat lobby parts,
 * instead I used currentUser and connectedWith props to set things up. In a real
 * app this approach is not suitable.
 */

const SplitApp = () => (
  <div className="d-flex">
    <div className="flex-1 m-5">
      <App currentUser={user1} connectedWith={user2} />
    </div>
    <div className="flex-1 m-5">
      <App currentUser={user2} connectedWith={user1} />
    </div>
  </div>
);

ReactDOM.render(<SplitApp />, document.getElementById('root'));
