import { IAction, IPayload } from './reducers';
import { ADD_MESSAGE_TO_CHAT, SEND_MESSAGE, SET_CONNECTION_DATA, NOTIFY_TYPING_STATUS } from './actionTypes';
import { SocketClient } from '../socketClient';
import { IUser } from '../../models/user';

export function addMessageToChat(message: string): IAction {
  const payload = {
    chat: { message },
  } as IPayload;

  return {
    payload,
    type: ADD_MESSAGE_TO_CHAT,
  };
}

export function sendMessage(message: string, to: string): IAction {
  SocketClient.sendMessage(message, to);

  const payload = {
    chat: { message },
  } as IPayload;

  return {
    payload,
    type: SEND_MESSAGE,
  };
}

export function setConnectionData(currentUser: IUser, connectedWith: IUser): IAction {
  const payload = { connectionInfo: { currentUser, connectedWith } } as IPayload;
  return {
    payload,
    type: SET_CONNECTION_DATA,
  };
}

export function notifyTypingStatus(isTyping: boolean): IAction {
  const payload = {
    userInteraction: { isTyping },
  } as IPayload;

  return {
    payload,
    type: NOTIFY_TYPING_STATUS,
  };
}
