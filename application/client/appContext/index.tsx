import React, { createContext, useReducer, useEffect } from 'react';
import {
  reducer as contextReducer,
  IAction,
  IChatMessage,
  IConnectionInfo,
  IUserInteraction,
  IPayload,
} from './reducers';
import { IUser } from '../../models/user';
import { setConnectionData } from './actionCreators';

export const CTX = createContext<IContext>({} as IContext);

function AppContext({ children, currentUser, connectedWith }: Props) {
  const { Provider } = CTX;

  useEffect(() => {
    dispatch(setConnectionData(currentUser, connectedWith));
  }, [currentUser.userId, connectedWith.userId]);

  const initialState: IPayload = {
    chat: {
      id: '1',
      message: '',
    },
    chatMessages: {
      some: [],
    },
    connectionInfo: {
      currentUser: {
        userId: '1',
        name: '',
        avatar: '',
      },
      connectedWith: {
        userId: '2',
        name: '',
        avatar: '',
      },
    },
    userInteraction: {
      isTyping: false,
      from: '',
      to: '',
    },
  };

  // @ts-ignore
  const [payload, dispatch] = useReducer(contextReducer, initialState);
  const { chatMessages, connectionInfo, userInteraction } = payload;
  const messages = chatMessages[connectionInfo.connectedWith.userId] || [];

  return <Provider value={{ messages, connectionInfo, userInteraction, dispatch }}>{children}</Provider>;
}

export { AppContext };

interface IContext {
  dispatch: React.Dispatch<IAction>;
  messages: IChatMessage[];
  connectionInfo: IConnectionInfo;
  userInteraction: IUserInteraction;
}

interface Props {
  children: JSX.Element[] | JSX.Element;
  currentUser: IUser;
  connectedWith: IUser;
}
