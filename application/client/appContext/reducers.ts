import { randomId } from '../utils/randomId';
import { IUser } from '../../models/user';
import { ADD_MESSAGE_TO_CHAT, SEND_MESSAGE, NOTIFY_TYPING_STATUS, SET_CONNECTION_DATA } from './actionTypes';

export const reducer = (state: IPayload, action: IAction) => {
  const { connectionInfo, chatMessages, userInteraction } = state;
  const { connectedWith, currentUser } = connectionInfo;
  const chatMessagesWith = chatMessages[connectedWith.userId] || [];
  const { message } = (action && action.payload && action.payload.chat) || '';

  switch (action.type) {
    case ADD_MESSAGE_TO_CHAT:
      return {
        connectionInfo,
        chatMessages: {
          [connectedWith.userId]: [
            { message, id: randomId(), isMe: false, timestamp: new Date().getTime() },
            ...chatMessagesWith,
          ],
        },
        userInteraction: {
          ...userInteraction,
          isTyping: false,
        },
      };

    case SEND_MESSAGE:
      return {
        connectionInfo,
        userInteraction,
        chatMessages: {
          [connectedWith.userId]: [
            { message, id: randomId(), isMe: true, timestamp: new Date().getTime() },
            ...chatMessagesWith,
          ],
        },
      };

    case NOTIFY_TYPING_STATUS:
      return {
        chatMessages,
        connectionInfo,
        userInteraction: {
          from: currentUser.name,
          to: connectedWith.name,
          isTyping: action.payload.userInteraction.isTyping,
        },
      };

    case SET_CONNECTION_DATA:
      return {
        chatMessages,
        userInteraction,
        connectionInfo: action.payload.connectionInfo,
      };

    default:
      return state;
  }
};

export interface IAction {
  type: 'ADD_MESSAGE_TO_CHAT' | 'SEND_MESSAGE' | 'SET_CONNECTION_DATA' | 'NOTIFY_TYPING_STATUS';
  payload: IPayload;
}

interface IChat {
  [key: string]: IChatMessage[];
}

export interface IChatMessage {
  id: string;
  message: string;
  isMe?: boolean;
  timestamp?: number;
}

export interface IPayload {
  chat: IChatMessage;
  chatMessages: IChat;
  connectionInfo: IConnectionInfo;
  userInteraction: IUserInteraction;
}

export interface IConnectionInfo {
  currentUser: IUser;
  connectedWith: IUser;
}

export interface IUserInteraction {
  isTyping: boolean;
  from: string;
  to: string;
}
