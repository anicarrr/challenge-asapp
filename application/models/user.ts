export interface IUser {
  userId: string;
  name: string;
  avatar: string;
}
