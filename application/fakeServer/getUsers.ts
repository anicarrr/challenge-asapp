import { randomId } from '../client/utils/randomId';
import robProfilePic from './assets/images/rob_avatar.png';
import lauraProfilePic from './assets/images/laura_avatar.png';
import { IUser } from '../models/user';

const user1: IUser = {
  userId: randomId(),
  name: 'Rob',
  avatar: robProfilePic,
};

const user2: IUser = {
  userId: randomId(),
  name: 'Laura',
  avatar: lauraProfilePic,
};

export { user1, user2 };
