class SocketMananger {
  public events: IEvent;
  constructor(events?: IEvent) {
    this.events = events || {};
  }

  public subscribe(name: string, cb: (...arg: any) => any) {
    // @ts-ignore
    (this.events[name] || (this.events[name] = [])).push(cb);

    return {
      unsubscribe: () =>
        // tslint:disable-next-line: no-bitwise
        this.events[name] && this.events[name].splice(this.events[name].indexOf(cb) >>> 0, 1),
    };
  }

  public emit(name: string, ...args: any[]): void {
    (this.events[name] || []).forEach((fn: any) => fn(...args));
  }
}

const instance = new SocketMananger();
Object.freeze(instance);

interface IEvent {
  [key: string]: [() => any];
}

export default instance;
