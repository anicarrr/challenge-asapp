import SocketManger from './socketManager';

SocketManger.subscribe('NEW_MESSAGE_EVENT', (payload: PayloadEvent) => {
  SocketManger.emit(`NEW_OUTSIDER_MESSAGE__${payload.to}`, payload.message);
});

SocketManger.subscribe('NOTIFY_TYPING_EVENT', (payload: PayloadEvent) => {
  SocketManger.emit(`SOMEONE_IS_TYPING_ME__${payload.to}`, payload.isTyping);
});

interface PayloadEvent {
  to: string;
  isTyping: boolean;
  message: string;
}
