const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackBar = require('webpackbar');
const autoprefixer = require('autoprefixer');

const isDevelopment = process.env.NODE_ENV !== 'production';
const basePath = './application/client';

module.exports = {
  mode: isDevelopment ? 'development' : 'production',
  devtool: isDevelopment && 'eval-source-map',
  entry: {
    app: `${basePath}/index.tsx`,
  },
  output: {
    path: path.join(__dirname, '/dist'),
    filename: isDevelopment ? '[name].js' : '[name].[contenthash].js',
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: isDevelopment,
      }),
      new OptimizeCSSAssetsPlugin({}),
    ],
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.scss', 'css'],
  },
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$|.scss$/,
        use: [
          isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
          {
            loader: 'typings-for-css-modules-loader',
            options: {
              modules: true,
              camelCase: true,
              namedExport: true,
              importLoaders: 1,
              localIdentName: '[local]___[hash:base64:5]',
            },
          },
          'sass-loader',
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: [autoprefixer],
            },
          },
        ],
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'assets/images',
          },
        },
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: `${basePath}/index.html`,
    }),
    new MiniCssExtractPlugin({
      filename: isDevelopment ? 'style.css' : '[name].[contenthash].css',
      chunkFilename: isDevelopment ? 'style.css' : '[name].[contenthash].css',
    }),
    new WebpackBar(),
  ],
};
