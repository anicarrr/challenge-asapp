module.exports = {
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  moduleNameMapper: {
    '\\.(css|less|scss|sss|styl)$': `${process.cwd()}/node_modules/jest-css-modules`,
  },
  testMatch: [`${process.cwd()}/application/client/**/__test__/**/*.test.tsx`],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
  resetModules: true,
  globals: {
    'ts-jest': {
      diagnostics: true,
      tsConfig: 'tsconfig.json',
    },
  },
};
